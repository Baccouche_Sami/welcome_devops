package fr.levitt.tp4.tp4web.dao;

import fr.levitt.tp4.tp4web.core.Gare;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GareRepository extends CrudRepository<Gare, String> {
}
