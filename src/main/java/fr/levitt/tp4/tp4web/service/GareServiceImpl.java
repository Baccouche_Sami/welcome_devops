package fr.levitt.tp4.tp4web.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.levitt.tp4.tp4web.cache.CustomCache;
import fr.levitt.tp4.tp4web.cache.MemoryCache;
import fr.levitt.tp4.tp4web.core.Gares;
import fr.levitt.tp4.tp4web.core.TableauAffichage;
import fr.levitt.tp4.tp4web.dao.GareRepository;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Primary
public class GareServiceImpl implements GareService {

    @Autowired
    private ObjectMapper mapper;

    @Value("${api.key}")
    private String apiKey;

    /**
     *
     * @param url
     * @return Response : la réponse si tout va bien, null sinon
     */
    private Response apiRequest(String url) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .header("Authorization",apiKey)
                .build();
        try {
            Response response = client.newCall(request).execute();
            return response;
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Autowired
    private CustomCache<Gares> cacheGares;

    @Autowired
    private GareRepository repo;

    @Override
    public Gares getGares() {

        if (cacheGares.getCache() != null) {
            return cacheGares.getCache();
        }

        Response response = apiRequest("https://api.navitia.io/v1/coverage/sncf/stop_areas?count=1000&");
        try {
            Gares gares = mapper.readValue(response.body().byteStream(), Gares.class);
            repo.saveAll(gares.getGares());
            cacheGares.updateCache(gares);
            return gares;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Cacheable("tableauAffichage")
    public TableauAffichage getTableauDeparts(String idGare) {
        System.out.println("Perdu "+idGare);
        Response response = apiRequest("https://api.navitia.io/v1/coverage/sncf/stop_areas/stop_area%3AOCE%3ASA%3A87444000/departures?from_datetime=20200303T144459&");
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            TableauAffichage tableau = mapper.readValue(response.body().byteStream(), TableauAffichage.class);

            return tableau;
        }
        catch (IOException e) {
            System.out.println("Erreur réseau");
            e.printStackTrace();
            return null;
        }
    }
}
